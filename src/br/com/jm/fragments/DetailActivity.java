package br.com.jm.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import br.com.jm.fragments.rss.RSSItem;

public class DetailActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		FragmentDetail fragmentDetail = (FragmentDetail) getSupportFragmentManager().findFragmentById(R.id.fragment_detail);

		if (fragmentDetail != null && fragmentDetail.isInLayout()){
		
			Bundle bundle = getIntent().getExtras();
			
			if (bundle != null){
				
				RSSItem rssItem = (RSSItem) bundle.getSerializable(RSSItem.RSS_ITEM_PARAM);			
				
				fragmentDetail.loadData(rssItem);

			}

		}

	}

}
