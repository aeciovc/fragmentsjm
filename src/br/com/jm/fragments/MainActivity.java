package br.com.jm.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import br.com.jm.fragments.rss.RSSItem;

public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final FragmentRSSList fragmentRSSList = (FragmentRSSList) getSupportFragmentManager().findFragmentById(R.id.fragment_list);
		
		if (fragmentRSSList != null && fragmentRSSList.isInLayout()){
			fragmentRSSList.loadNews(new FragmentRSSList.ItemListener() {
				
				@Override
				public void onClickItem(RSSItem item) {
					
					if (fragmentRSSList.isInTablet()){
						
						FragmentDetail fragmentDetail = (FragmentDetail) getSupportFragmentManager().findFragmentById(R.id.fragment_detail);
						if (fragmentDetail != null && fragmentDetail.isInLayout()){
							fragmentDetail.loadData(item);
						}
						
					}else{
						Intent intent = new Intent(MainActivity.this, DetailActivity.class);
						intent.putExtra(RSSItem.RSS_ITEM_PARAM, item);
						startActivity(intent);
					}
				}
			});
		}
		
	}
}
