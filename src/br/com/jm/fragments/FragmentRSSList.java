package br.com.jm.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import br.com.jm.fragments.rss.RSS;
import br.com.jm.fragments.rss.RSS.ResultListener;
import br.com.jm.fragments.rss.RSSItem;

public class FragmentRSSList extends Fragment implements ResultListener {

	private ItemListener mItemListener;
	
	public interface ItemListener {
        public void onClickItem(RSSItem item);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_rss_fragment, container,
				false);
		return view;
	}

	public void loadNews(ItemListener itemListener){
		this.mItemListener = itemListener;
		
		RSS rss = new RSS();
		rss.getNews("http://g1.globo.com/dynamo/rss2.xml", this);
	}
	
	public boolean isInTablet(){
		if (getActivity().getResources().getBoolean(R.bool.isTablet))
			return true;
		else
			return false;
	}

	@Override
	public void onResult(ArrayList<RSSItem> resultList) {
		
		ArrayAdapter<RSSItem> adapter = new ArrayAdapter<RSSItem>(getActivity(), android.R.layout.simple_list_item_1, resultList);
		
		ListView listView = (ListView) getView().findViewById(R.fragmentView.listView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long arg3) {
				RSSItem rssItem = (RSSItem) adapterView.getItemAtPosition(position);
				
				mItemListener.onClickItem(rssItem);
			}
		});
		
	}
	
}
