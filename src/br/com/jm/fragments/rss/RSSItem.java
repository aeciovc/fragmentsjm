package br.com.jm.fragments.rss;

import java.io.Serializable;

public class RSSItem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String RSS_ITEM_PARAM = "RSS_ITEM_PARAM";
	
	private String title;
	private String url;

	public RSSItem(String title, String url){
		this.title = title;
		this.url = url;
	}
	
	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}
	
	@Override
	public String toString() {
		return getTitle();
	}
	
}
