package br.com.jm.fragments.rss;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.os.AsyncTask;


public class RSS {
	
		
	public interface ResultListener {
        public void onResult(ArrayList<RSSItem> list);
    }
	
	public void getNews(String url, ResultListener resultListener){
		
		AsyncTaskRSS asyncTaskRSS = new RSS.AsyncTaskRSS();
		asyncTaskRSS.execute(url, resultListener);
		
	}
	
	class AsyncTaskRSS extends AsyncTask<Object, Void, ArrayList<RSSItem>>{
	
		ResultListener resultListener;
		
		@Override
		protected ArrayList<RSSItem> doInBackground(Object... params) {
			
			String url = (String) params[0];
			resultListener = (ResultListener) params[1];
			
			Document doc = null;
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
			try {
				db = dbf.newDocumentBuilder();
			
				doc = db.parse(url);

			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			}
			
			NodeList listItem = doc.getElementsByTagName("item");
			ArrayList<RSSItem> itemList = new ArrayList<RSSItem>();

			for(int i = 0; i < listItem.getLength(); i++){
				
				//Title
				String title = listItem.item(i).getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
				
				//Link
				String urlItem = listItem.item(i).getChildNodes().item(1).getChildNodes().item(0).getNodeValue();
				
				itemList.add(new RSSItem(title, urlItem));
			}
			
			
			return itemList;
		}
		
		@Override
		protected void onPostExecute(ArrayList<RSSItem> resultList) {
			
			resultListener.onResult(resultList);
			
			super.onPostExecute(resultList);
		}
		
	}
		
}
